var vga_colors = [
    [0.0, 0.0, 0.0],
    [0.0, 0.0, 0.5],
    [0.0, 0.5, 0.0],
    [0.0, 0.5, 0.5],
    [0.5, 0.0, 0.0],
    [0.5, 0.0, 0.5],
    [0.5, 0.5, 0.0],
    [0.5, 0.5, 0.5],
    [0.75, 0.75, 0.75], /* Idea:  Make this color customizable by user. */
    [0.0, 0.0, 1.0],
    [0.0, 1.0, 0.0],
    [0.0, 1.0, 1.0],
    [1.0, 0.0, 0.0],
    [1.0, 0.0, 1.0],
    [1.0, 1.0, 0.0],
    [1.0, 1.0, 1.0]
]; /* I1R1G1B1 */

/* one-time intialized, never updated */
var cols;
var rows;

var text = "";
var cursor_blink_active = 1;

/* EXTERNAL */
var c_w = chardims[0];
var c_h = chardims[1];

function bputc(ch) {
    "use strict";
    var bitmap;

    bitmap = rasters[ch.charCodeAt(0)];
    if (!bitmap) {
        bitmap = unimplemented_code_point;
    }
    glBitmap(c_w, c_h, 0, 0, c_w, 0, bitmap);
}

/*
 * Sometimes the 2-D coordinates in the VT rectangle will differ from the
 * linear index into the user-input text buffer to be written.  This would be
 * due primarily to line feeds entered by the user and partial line input.
 */
function xy_to_index(x, y) {
    "use strict";
    var current_line; /* corrects OpenGL y-coord to top-bottom scan order */

    current_line = rows - 1 - y;
    return (cols * current_line + x);
}

var vga_bg;
function draw_bg(rows, cols) {
    "use strict";
    var char_x;
    var char_y;
    var color;
    var i = 0;
    var string_length = vga_bg.length;

    glClearColor(0, 0, 0, 0); /* Let CSS decide terminal background color. */
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_SCISSOR_TEST);
    while (i < string_length) {
        char_x = i % cols;
        char_y = rows - 1 - (i - char_x) / cols;
        color = vga_colors[parseInt(vga_bg[i], 16)];

        glScissor(char_x * c_w, char_y * c_h, c_w, c_h);
        glClearColor(color[0], color[1], color[2], 1.0);
        glClear(GL_COLOR_BUFFER_BIT);
        i += 1;
    }
    glDisable(GL_SCISSOR_TEST);
    if (i === 1) { /* User can override CSS background color with 1 nybble. */
        glClear(GL_COLOR_BUFFER_BIT);
    }
}

var vga_fg;
function draw_vga_screen() {
    "use strict";
    var color;
    var char_current;
    var char_vt_offset;
    var char_x = 0;
    var char_y = rows - 1;
    var i = 0;

    draw_bg(rows, cols);
    glColor4f(1.0, 1.0, 1.0, 1);
    glRasterPos2f(0, c_h * char_y);

    if (!text) {
        cursor_blink_active = -cursor_blink_active;
        if (cursor_blink_active < 0) {
            return;
        }
        color = vga_colors[parseInt(vga_fg[0], 16)];
        glColor4f(color[0], color[1], color[2], color[3]);
        glRasterPos2f(0, c_h * char_y);
        bputc("_");
        return;
    }

    while (char_y >= 12) {
        char_x = 0;
        glRasterPos2f(0, c_h * char_y);
        while (char_x < cols) {
            char_vt_offset = xy_to_index(char_x, char_y);
            color = vga_colors[parseInt(vga_fg[char_vt_offset], 16)];
            glColor4f(color[0], color[1], color[2], 1.0);
            glRasterPos2f(char_x * c_w, char_y * c_h);
            char_current = text[i];
            char_x += 1;
            i += 1;

            if (char_x === cols && text[i].charCodeAt(0) === 10) {
                i += 1;
            }

            if (char_current.charCodeAt(0) === 10) {
                char_x = cols; /* break; */
            } else {
                bputc(char_current);
            }
        }
        char_y -= 1;
    }

    while (char_y >= 0 && i < text.length) {
        const line_no = parseInt(text[i] + text[i + 1] + text[i + 2]);
        char_x = 0;
        if (line_no > 0) {
            glEnable(GL_SCISSOR_TEST);
            glScissor(0, char_y * c_h, 3 * c_w, c_h);
            glClearColor(0.0, 0.5, 0.0, 1);
            glClear(GL_COLOR_BUFFER_BIT);
            glDisable(GL_SCISSOR_TEST);
        }

        glRasterPos2f(0, c_h * char_y);
        while (char_x < cols) {
            char_vt_offset = xy_to_index(char_x, char_y);
            color = vga_colors[parseInt(vga_fg[char_vt_offset], 16)];
            glColor4f(color[0], color[1], color[2], 1.0);
            glRasterPos2f(char_x * c_w, char_y * c_h);
            char_current = text[i];
            char_x += 1;
            i += 1;

            if (char_x === cols && text[i].charCodeAt(0) === 10) {
                i += 1;
            }

            if (char_current.charCodeAt(0) === 10) {
                char_x = cols; /* break; */
            } else {
                bputc(char_current);
            }
        }
        char_y -= 1;
    }

}

function string_from_URI(name, default_value) {
    "use strict";
    var offset;
    var offset1;
    var offset2;
    var href = document.location.href;

    offset1 = href.indexOf("?" + name + "=");
    offset2 = href.lastIndexOf("&" + name + "=");
    offset = (
        (offset1 < offset2)
        ? offset2
        : offset1
    );

    if (offset < 0) {
        return (default_value);
    }
    offset += 1 + name.length + 1; /* Jump past ?/&, (name), and '='. */
    offset2 = href.indexOf("&", offset);
    if (offset2 < 0) {
        offset2 = href.length;
    }
    return href.substring(offset, offset2);
}
function number_from_URI(name, default_value) {
    "use strict";
    var tentative_value;

    tentative_value = string_from_URI(name, default_value);
    tentative_value = parseInt(tentative_value);
    if (tentative_value.isNaN || Number(tentative_value).isNaN) {
        return (default_value);
    }
    return (tentative_value);
}

function unpack_modes(pattern) {
    "use strict";
    var current;
    var previous;
    var figures;
    var decompressed = "";
    var i = 0 + 1;
    var repeat_by;

    previous = pattern[0];
    if (!pattern || !previous) {
        return ("");
    }
    if (pattern[1] !== "x") {
        decompressed += previous;
    }
    while (i < pattern.length) {
        current = pattern[i];
        if (current === "x") {
            figures = "";
            i += 1;
            while (pattern[i] !== "." && i < pattern.length) {
                figures += pattern[i];
                i += 1;
            }
            i += 1;
            repeat_by = parseInt(figures, 10);
            while (repeat_by > 0) {
                decompressed += previous;
                repeat_by -= 1;
            }
        } else {
            i += 1;
            if (pattern[i] !== "x") {
                decompressed += current;
            }
        }
        previous = current;
    }
    return (decompressed);
}

function fg_unpack() {
    "use strict";
    var totals = ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A"];
    const adj = "C"; /* 0xC = 0b1100 or bright red - adjusted price color */

    if (document.getElementById("cost1").checked) {
        totals[0] = adj;
    }
    if (document.getElementById("cost2").checked) {
        totals[1] = adj;
    }
    if (document.getElementById("cost3").checked) {
        totals[2] = adj;
    }
    if (document.getElementById("cost4").checked) {
        totals[3] = adj;
    }
    if (document.getElementById("cost5").checked) {
        totals[4] = adj;
    }
    if (document.getElementById("cost6").checked) {
        totals[5] = adj;
    }
    if (document.getElementById("cost7").checked) {
        totals[6] = adj;
    }
    if (document.getElementById("cost8").checked) {
        totals[7] = adj;
    }
    if (document.getElementById("cost9").checked) {
        totals[8] = adj;
    }
    if (document.getElementById("cost10").checked) {
        totals[9] = adj;
    }

    vga_fg = unpack_modes(
        "Ex31.Ax21.Fx80." +
        "Ex31.Ax21.Fx80." +
        "Ex36.Ax16.Fx80." +
        "Ex31.Bx1.Ax20.Fx80." +
        "Ex31.Bx1.Ax20.Fx80." +
        "Ex31.Bx1.Ax20.Fx80." +
        "Bx2.Ax18.Bx4.Ax10.Bx4.Ax14.Fx80." +
        "Bx2.Ax4.Bx2.Ax16.Ax5.Bx3.Ax6.Bx4.Ax10.Fx80." +
        "Ax52.Fx80." +
        "Bx4.Ax2.Bx4.Ax2.Bx3.Ax14.Bx11.Ax12.Fx80." +
        "Ex13.Bx8.Ex13.Bx4.Ex10.Bx4.Ex23.Bx5.Ex14.Fx38." +
        "Fx132." +
        "Bx56.Cx8.Bx68." +
        "Fx4.Ax88." + totals[0] + "x10.Ax30." +
        "Fx4.Ax88." + totals[1] + "x10.Ax30." +
        "Fx4.Ax88." + totals[2] + "x10.Ax30." +
        "Fx4.Ax88." + totals[3] + "x10.Ax30." +
        "Fx4.Ax88." + totals[4] + "x10.Ax30." +
        "Fx4.Ax88." + totals[5] + "x10.Ax30." +
        "Fx4.Ax88." + totals[6] + "x10.Ax30." +
        "Fx4.Ax88." + totals[7] + "x10.Ax30." +
        "Fx4.Ax88." + totals[8] + "x10.Ax30." +
        "Fx4.Ax88." + totals[9] + "x10.Ax30." +
        "Fx132"
    );
}

function main_GL() {
    "use strict";

    cols = number_from_URI("w", 132);
    rows = number_from_URI("h", 24);
    document.getElementById("vt").cols = cols;
    document.getElementById("vt").rows = rows;

    document.getElementById("GL_canvas").width = cols * c_w;
    document.getElementById("GL_canvas").height = rows * c_h;
    if (GL_get_context(document, "GL_canvas") === null) {
        window.alert("Failed to initialize WebGL.");
        return;
    }
    glViewport(0, 0, cols * c_w, rows * c_h);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    text = string_from_URI("t", "");
    vga_bg = unpack_modes(
        "0x52.4x80." +
        "0x52.4x80." +
        "0x52.4x80." +
        "0x52.4x80." +
        "0x52.4x80." +
        "0x52.4x80." +
        "0x52.4x80." +
        "0x52.4x80." +
        "0x52.4x80." +
        "0x52.4x80." +
        "0x94.Cx38." +
        "0x132." +
        "0x132." +
        "0x3.0x129." +
        "0x3.0x129." +
        "0x3.0x129." +
        "0x3.0x129." +
        "0x3.0x129." +
        "0x3.0x129." +
        "0x3.0x129." +
        "0x3.0x129." +
        "0x3.0x129." +
        "0x3.0x129." +
        "0x132"
    );
    fg_unpack();
    setInterval(draw_vga_screen, 750);
}
